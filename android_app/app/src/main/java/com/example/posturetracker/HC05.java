package com.example.posturetracker;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class HC05 {
    private static final UUID mUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String macAddress = "98:D3:51:FD:7A:77";
    private BluetoothSocket btSocket;
    OutputStream outputStream;
    InputStream inputStream;

    HC05() throws IOException {
        initialize();
    }

    private boolean initialize() throws IOException {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) return false;
        BluetoothDevice hc05 = btAdapter.getRemoteDevice(macAddress);

        btSocket = hc05.createRfcommSocketToServiceRecord(mUUID);

        outputStream = btSocket.getOutputStream();
        inputStream = btSocket.getInputStream();
        return true;
    }

    public boolean connect() throws IOException {
        if (btSocket == null && !initialize()) return false;
        btSocket.connect();
        return true;
    }

    public void disconnect() throws IOException {
        btSocket.close();
        btSocket = null;
    }

    public boolean isConnected() {
        return btSocket != null && btSocket.isConnected();
    }

    public void clear() throws IOException {
        inputStream.skip(inputStream.available());
    }

    public void write(int i) throws IOException {
        outputStream.write(i);
    }

    public byte read() throws IOException {
        return (byte) inputStream.read();
    }
}