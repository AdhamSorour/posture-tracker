package com.example.posturetracker;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Runnable {
    HC05 hc05;
    double yawRef, pitchRef, rollRef;
    boolean print = true, calibrate = true, running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            hc05 = new HC05();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void toggleConnection(View view) {
        Button connectButton = (Button) findViewById(R.id.btnConnect);
        TextView statusBox = (TextView) findViewById(R.id.txtStatus);

        if (connectButton.getText().toString().matches("Connect")) {
            System.out.println("Start Connection");
            try {
                connectHC05();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (hc05.isConnected()) statusBox.setText("Connected");
            else System.out.println("HC05 connection unsuccessful");
        } else {
            System.out.println("Disconnect");
            try {
                disconnectHCO5();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!hc05.isConnected()) statusBox.setText("Disconnected");
        }
    }

    private void connectHC05() throws IOException {
        Button connectButton = (Button) findViewById(R.id.btnConnect);
        Button calibrateButton = (Button) findViewById(R.id.btnCalibrate);
        if (!hc05.connect()) return;
        connectButton.setText("Disconnect");
        calibrateButton.setEnabled(true);
    }

    private void disconnectHCO5() throws IOException {
        Button connectButton = (Button) findViewById(R.id.btnConnect);
        Button calibrateButton = (Button) findViewById(R.id.btnCalibrate);
        hc05.disconnect();
        calibrateButton.setEnabled(false);
        connectButton.setText("Connect");
    }

    public void calibrate(View view) {
        Button trackButton = (Button) findViewById(R.id.btnTrack);
        Button calibrateButton = (Button) findViewById(R.id.btnCalibrate);
        TextView statusBox = (TextView) findViewById(R.id.txtStatus);

        System.out.println("Start Calibration");
        try {
            getReferenceValues();
        } catch (IOException e) {
            e.printStackTrace();
        }
        trackButton.setEnabled(true);
        calibrateButton.setText("Recalibrate");
        statusBox.setText("Calibrated");
    }

    private void getReferenceValues() throws IOException {
        boolean overflow = false;
        long startTime = 0;
        int msgCount = 0;

        if (hc05.isConnected()) {
            String valueString = "";
            String[] values;
            List<Double> yawValues = new ArrayList<Double>();
            List<Double> pitchValues = new ArrayList<Double>();
            List<Double> rollValues = new ArrayList<Double>();

            hc05.clear();
            startTime = System.currentTimeMillis();

            while (System.currentTimeMillis() - startTime < 5000) {
                hc05.write(1);
                char ch = (char) hc05.read();

                if (ch == '!') {
                    overflow = true;
                } else if (ch == '$') {
                    values = valueString.split(",");

                    if (values.length == 3 && !overflow) {
                        double yaw = Double.parseDouble(values[0]);
                        double pitch = Double.parseDouble(values[1]);
                        double roll = Double.parseDouble(values[2]);
                        if (values[0].length() < 9) yawValues.add(yaw);
                        if (values[1].length() < 9) pitchValues.add(pitch);
                        if (values[2].length() < 9) rollValues.add(roll);
                        System.out.println("yaw:" + values[0] + " pitch:" + values[1] + " roll:" + values[2]);
                        msgCount++;
                    }

                    valueString = "";
                    overflow = false;
                    continue;
                }
                valueString += ch;
            }

            yawRef = average(yawValues);
            pitchRef = average(pitchValues);
            rollRef = average(rollValues);
            System.out.println("[Reference Values] yaw:" + yawRef + " pitch:" + pitchRef + " roll:" + rollRef);
            System.out.println("Count: " + msgCount);
        }
    }

    private double average(List<Double> list) {
        List<Double> cleanValues = removeOutliers(list);
        double sum = 0;
        if(!cleanValues.isEmpty()) {
            for (Double value : cleanValues) {
                sum += value;
            }
        }
        return sum / cleanValues.size();
    }

    private static List<Double> removeOutliers(List<Double> input) {
        Collections.sort(input);
        List<Double> output = new ArrayList<Double>();
        for (int i = input.size()/4; i < input.size() - input.size()/4; i++) {
            output.add(input.get(i));
        }
        return output;
    }

    public void toggleTracking(View view) {
        Button trackButton = (Button) findViewById(R.id.btnTrack);
        TextView statusBox = (TextView) findViewById(R.id.txtStatus);

        if (trackButton.getText().toString().matches("Track")) {
            System.out.println("Start Tracking");
            trackButton.setText("Stop");
            statusBox.setText("Tracking");
            new Thread(this).start(); //Start reading data
        } else {
            System.out.println("Stop Tracking");
            trackButton.setText("Track");
            statusBox.setText("Calibrated");
            running = false; //Stop reading data
        }
    }

    public void run() {
        final ImageView uprightImg = (ImageView) findViewById(R.id.imgUpright);
        final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (hc05.isConnected()) {
            running = true;
            boolean overflow = false;

            try {
                hc05.clear();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String valueString = "";
            String[] values;

            while (running) {
                char ch = 0;
                try {
                    hc05.write(1);
                    ch = (char) hc05.read();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (ch == '!') {
                    overflow = true;
                } else if (ch == '$') {
                    values = valueString.split(",");
                    if (values.length == 3 && !overflow && print) {
                        final double yaw = Double.parseDouble(values[0]);
                        final double pitch = Double.parseDouble(values[1]);
                        final double roll = Double.parseDouble(values[2]);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (yawRef-yaw >= 15 && uprightImg.getVisibility() == View.VISIBLE) {
                                    uprightImg.setVisibility(View.INVISIBLE);
                                    vibrator.vibrate(100);
                                } else if (yawRef-yaw < 15 && uprightImg.getVisibility() == View.INVISIBLE) {
                                    uprightImg.setVisibility(View.VISIBLE);
                                }
                            }
                        });

                        System.out.println("yaw:" + yaw + " pitch:" + pitch + " roll:" + roll);
                    }

                    valueString = "";
                    overflow = false;
                    continue;
                }
                valueString += ch;
            }
        }
    }
}