# Posture Tracker

A device that tracks the user's posture, accompanied with an android app that visually displays the current posture and notifies the user when their posture is out of a specified range.

## Description
The device used in this project is built using the following components:
- 1 Arduino Nano
- 1 Bluetooth Module (HC-05)
- 1 IMU Sensor (MPU-6050)
- 1 9 Volt Battery (rechargeable)
- 1 Push Button Switch (optional)

The circuit layout connecting the components:

![](images/schematic.png)

The Arduino has a 5V high logic level, while the HC-05 has a 3.3V high logic level. The two resistors divide the 5V coming out of the Arduino's D9 pin to create a voltage of 3.3V at the RX pin of the HC-05. The Arduino will be able to read the 3.3V coming out of the TX pin of the HC-05 as a logical high, so nothing needs to be done to the HC-05 output.

The optional push button can be connected between the VIN pin of the Arduino and the +ve terminal of the battery to easily switch the power on/off.


In order to edit, build & deploy the app to your phone, you would need to install Android Studio. An Android phone is required.

## Usage
When you have the assembled device and the mobile app ready, follow these steps to start tracking your posture:

**1. Place the device on your back**

![](images/placement.gif)

You will need a way to stick the device to your back, I used double sided tape. Make sure you place it somewhere that is comfortable to sit for prolonged periods of time.

**2. Set up the device through the mobile app**

Open the app & connect to the device

<img src="images/screen_capture_2.jpg"  width="20%" height="20%"> 

Stand upright in the desired position, then press "Calibrate". Keep standing still until the "Track" button is enabled

<img src="images/screen_capture_3.jpg"  width="20%" height="20%"> 

After calibration, press the "Track" button to start tracking your posture

<img src="images/screen_capture_4.jpg"  width="20%" height="20%"> 

You are now all set up! 

**3. Use the device to keep an upright posture**

![](images/demo.gif)


## Project status
Development for the project is currently on hold.
I am open to contributions, you can reach me at adham.sorour@hotmail.com

